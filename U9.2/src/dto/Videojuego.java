package dto;

import service.Entregable;

public class Videojuego implements Entregable{
	
	protected static int max_horas=0;
	
	//ATRIBUTOS
	protected String titulo;
	
	protected int horas_estimadas;
	
	protected boolean entregado;
	
	protected String genero;
	
	protected String compa�ia;
	
	//CONSTRUCTORES
	//GENERO EL CONSTRUCTOR POR DEFECTO
	public Videojuego() {
		this.titulo="";
		this.horas_estimadas=10;
		this.entregado=false;
		this.genero="";
		this.compa�ia="";
	}
	
	public Videojuego(String titulo, int horas_estimadas) {
		this.titulo=titulo;
		this.horas_estimadas=horas_estimadas;
		this.entregado=false;
		this.genero="";
		this.compa�ia="";
	}

	
	//GETTERS
	public String getTitulo() {
		return titulo;
	}

	public int getHoras_estimadas() {
		return horas_estimadas;
	}

	public String getGenero() {
		return genero;
	}

	public String getCompa�ia() {
		return compa�ia;
	}
	
	
	//SETTERS
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setHoras_estimadas(int horas_estimadas) {
		this.horas_estimadas = horas_estimadas;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setCompa�ia(String compa�ia) {
		this.compa�ia = compa�ia;
	}
	
	@Override
	public String toString() {
		return "Videojuego [titulo=" + titulo + ", horas_estimadas=" + horas_estimadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", compa�ia=" + compa�ia + "]";
	}

	//M�TODOS ENTREGABLE
	public void entregar() {
		entregado = true;
	}
	
	public void devolver() {
		entregado = false;
	}
	
	public boolean isEntregado() {
		return entregado;
	}
	
	public boolean compareTo(Videojuego videojuego) {
		
		if (this.horas_estimadas > videojuego.getHoras_estimadas()) 
			return true;
		
		return false;
	
	
}
}
