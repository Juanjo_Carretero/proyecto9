package dto;

import service.Entregable;

public class Serie implements Entregable{
	
	protected static int max_temporadas=0;
	
	//ATRIBUTO
	protected String titulo;
	
	protected int num_temporadas;
	
	protected boolean entregado;
	
	protected String genero;
	
	protected String creador;
	
	//CONSTRUCTORES
	//GENERO EL CONSTRUCTOR POR DEFECTO
	public Serie() {
		this.creador="";
		this.titulo="";
		this.num_temporadas=3;
		this.entregado=false;
		this.genero="";
	}
	
	public Serie(String creador, String titulo) {
		this.creador=creador;
		this.titulo=titulo;
		this.num_temporadas=3;
		this.entregado=false;
		this.genero="";
	}
	
	public Serie(String creador, String titulo, int num_temporadas, String genero) {
		this.creador=creador;
		this.titulo=titulo;
		this.num_temporadas=num_temporadas;
		this.genero=genero;
	}
	
	//GETTERS
	public String getTitulo() {
		return titulo;
	}

	public int getNum_temporadas() {
		return num_temporadas;
	}

	public String getGenero() {
		return genero;
	}

	public String getCreador() {
		return creador;
	}
	
	
	//SETTERS
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setNum_temporadas(int num_temporadas) {
		this.num_temporadas = num_temporadas;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", num_temporadas=" + num_temporadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}
	
	
	//M�TODOS ENTREGABLE
	public void entregar() {
		entregado = true;
	}
	
	public void devolver() {
		entregado = false;
	}
	
	public boolean isEntregado() {
		return entregado;
	}
	
	public boolean compareTo(Serie serie) {
		if (this.num_temporadas > serie.getNum_temporadas()) 
			return true;
		
		return false;
	}
	
	
	
}

