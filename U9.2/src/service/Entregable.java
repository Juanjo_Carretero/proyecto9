package service;

public interface Entregable {
	
	public void entregar();
	
	public void devolver();
	
	public boolean isEntregado();
	
}