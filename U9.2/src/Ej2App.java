import java.util.ArrayList;

import dto.Serie;

import dto.Videojuego;

public class Ej2App{
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//GENERACION DE ARRAYS Y VARIABLES
		ArrayList<Serie> array=new ArrayList<Serie>();
		ArrayList<Videojuego> array2=new ArrayList<Videojuego>();
		int totalentregados=0;
		
		
		//LLENO EL ARRAY SERIE DE SERIES
		Serie serie1 = new Serie();
		Serie serie2 = new Serie("Hola", "Saludos");
		Serie serie3 = new Serie("Adi�s", "Despidos");
		Serie serie4 = new Serie();
		Serie serie5 = new Serie();
		Serie serie6 = new Serie("Nose", "Dark", 3, "ciencia ficci�n");
		
		array.add(serie1);
		array.add(serie2);
		array.add(serie3);
		array.add(serie4);
		array.add(serie5);
		array.add(serie6);
		
		//LLENO EL ARRAY DE JUEGOS DE JUEGOS
		Videojuego juego1 = new Videojuego();
		Videojuego juego2 = new Videojuego("Fallout",40);
		Videojuego juego3 = new Videojuego("Call Of Duty", 14);
		Videojuego juego4 = new Videojuego();
		Videojuego juego5 = new Videojuego("League Of Legends", 1000);
		Videojuego juego6 = new Videojuego();
		
		array2.add(juego1);
		array2.add(juego2);
		array2.add(juego3);
		array2.add(juego4);
		array2.add(juego5);
		array2.add(juego6);
		
		//ENTREGA DE SERIES Y JUEGOS
		juego1.entregar();
		juego5.entregar();
		
		serie5.entregar();
		serie4.entregar();
		serie2.entregar();
		
		//CUENTO CUANTAS SERIES Y JUEGOS HAY ENTREGADOS
		for (int i = 0; i < array.size(); i++) {
			if (array.get(i).isEntregado()) {
				totalentregados++;
			}
		}
		
		for (int i = 0; i < array2.size(); i++) {
			if (array2.get(i).isEntregado()) {
				totalentregados++;
			}
		}
		
		System.out.println("El total de entregados �s: "+totalentregados);
		
		//BUSCO LA SERIE MAS LARGA Y JUEGO MAS LARGAS
		Serie seriemas = new Serie("Prueba","prueba",1,"prueba");
		Videojuego juegomas = new Videojuego("prueba", 1);
		
		for (int i = 0; i < array.size(); i++) {
			if (!seriemas.compareTo(array.get(i))) {
				 seriemas = array.get(i);
			}
		}
		
		for (int i = 0; i < array2.size(); i++) {
			if (!juegomas.compareTo(array2.get(i))) {
				 juegomas = array2.get(i);
			}
		}
		
		System.out.println("Titulo serie con m�s temporadas: "+seriemas.getTitulo());
		System.out.println("Titulo juego con m�s horas estimadas: "+juegomas.getTitulo());
	}

}

