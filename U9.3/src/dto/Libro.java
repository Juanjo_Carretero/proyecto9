package dto;

public final class Libro {
	
	private String isbn;
	private String titulo;
	private String autor;
	private int numPaginas;
	
	
	
	/**
	 * 
	 */
	public Libro() {
		this.isbn="987-92-95055-02-5";
		this.titulo="Lo que el viento se llevo";
		this.autor= "Margaret Mitchell";
		this.numPaginas=300;
	}
	/**
	 * @param isbn: ISBN del libro
	 * @param titulo: titulo de la obra
	 * @param autor: escritor del libro
	 * @param numPaginas: numero de paginas
	 */
	public Libro(String isbn, String titulo, String autor, int numPaginas) {
		super();
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.numPaginas = numPaginas;
	}
	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}
	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}
	/**
	 * @param autor the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}
	/**
	 * @return the numPaginas
	 */
	public int getNumPaginas() {
		return numPaginas;
	}
	/**
	 * @param numPaginas the numPaginas to set
	 */
	public void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}
	@Override
	public String toString() {
		return "El libro \""+this.titulo+"\" con ISBN "+this.isbn+" creado por "
				+this.autor+" tiene "+this.numPaginas+" paginas";
	}
	
	/**
	 * Dado un libro como parametro indica cual tiene mas 
	 * paginas o en caso de tener igual tambien
	 * @param l: libro a comparar
	 * @return 1 si el libro pasado por parametro es mas peque�o
	 * 0 si los dos tienen las mismas paginas
	 * -1 si el libro pasado por parametro es mas grande
	 */
	public int compareNumPags(Libro l) {
		if(this.numPaginas> l.getNumPaginas())
			return 1;
		else {
			if(this.numPaginas==l.getNumPaginas())
				return 0;
			else
				return -1;
		}
	}
	
	

}

