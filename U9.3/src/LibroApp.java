import dto.Libro;

public class LibroApp {

	public static void main(String[] args) {

		Libro l1= new Libro("253-15-1532-89-4", "Doble juego","Ken Follet", 468);
		Libro l2= new Libro("157-25-16581-32-5", "La caida de los gigantes", "Ken Follet", 300);
		
		System.out.println(l1);
		System.out.println(l2);
		
		int mayor= l1.compareNumPags(l2);
		switch (mayor) {
		case -1:
			 System.out.println(l2.getTitulo()+" tiene mas paginas.");
			break;
		case 0:
			 System.out.println("Los dos libros tienen las mismas paginas");
			break;
		case 1:
			 System.out.println(l1.getTitulo()+" tiene mas paginas.");
			break;
		
		}

	}

}
