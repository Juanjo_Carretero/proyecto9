package dto;

public class Pelicula {

	private String titulo;
	private int duracion;
	private int minAge;
	private String director;
	
	/**
	 * 
	 */
	public Pelicula() {
		super();
		this.titulo=null;
		this.duracion=0;
		this.minAge=0;
		this.director=null;
	}

	/**
	 * @param titulo: titulo de la pelicula
	 * @param duracion: duracion en minutos de la pelicula
	 * @param minAge: edad minima para ver la pelicula
	 * @param director: director de la pelicula
	 */
	public Pelicula(String titulo, int duracion, int minAge, String director) {
		super();
		this.titulo = titulo;
		if(duracion>0)
			this.duracion = duracion;
		else
			this.duracion =100;
		if(minAge>0)
			this.minAge = minAge;
		else
			this.minAge = 3;
		this.director = director;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the duracion
	 */
	public int getDuracion() {
		return duracion;
	}

	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(int duracion) {
		if(duracion>0)
			this.duracion = duracion;
		else
			this.duracion =100;
	}

	/**
	 * @return the minAge
	 */
	public int getMinAge() {
		return minAge;
	}

	/**
	 * @param minAge the minAge to set
	 */
	public void setMinAge(int minAge) {
		if(minAge>0)
			this.minAge = minAge;
		else
			this.minAge = 3;
	}

	/**
	 * @return the director
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * @param director the director to set
	 */
	public void setDirector(String director) {
		this.director = director;
	}
	
	
	
	
	
}
