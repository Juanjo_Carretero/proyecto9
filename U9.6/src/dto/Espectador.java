package dto;

import java.util.ArrayList;
import java.util.HashMap;

public class Espectador {
	
	
	
	private String nombre;
	private int edad;
	private double dinero;
	private String asiento;
	
	
	/**
	 * 
	 */
	public Espectador() {
		super();
		this.nombre="NAME";
		this.edad=18;
		this.dinero=10;
		this.asiento=null;
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param nombre: nombre del espectador
	 * @param edad: edad del espectador
	 * @param dinero: dinero del que dispone el espectador
	 */
	public Espectador(String nombre, int edad, double dinero) {
		super();
		this.nombre = nombre;
		if(edad>0)
			this.edad = edad;
		else
			this.edad=18;
		if(dinero>0)
			this.dinero = dinero;
		else
			this.dinero = 10;
		this.asiento=null;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}
	/**
	 * @param edad the edad to set
	 */
	public void setEdad(int edad) {
		if(edad>0)
			this.edad = edad;
		else
			this.edad=18;
	}
	/**
	 * @return the dinero
	 */
	public double getDinero() {
		return dinero;
	}
	/**
	 * @param dinero the dinero to set
	 */
	public void setDinero(double dinero) {
		if(dinero>0)
			this.dinero = dinero;
		else
			this.dinero = 10;
	}
	/**
	 * @return the asiento
	 */
	public String getAsiento() {
		return asiento;
	}
	/**
	 * @param asiento the asiento to set
	 */
	public void setAsiento(String asiento) {
		this.asiento = asiento;
	}
	@Override
	public String toString() {
		return "Espectador [nombre=" + nombre + ", edad=" + edad + ", dinero=" + String.format("%.2f",dinero) + ", asiento=" + asiento + "]";
	}
	
	
	
	
	
	
	
}
