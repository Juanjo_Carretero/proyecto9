package dto;

public class Nombres {

	public final static char HOMBRE='H';
	public final static char MUJER='M';
	
	public final static String nombresH[]= {"MARC", "DANIEL", "JESUS", "JOSE", "JUAN", "PEDRO", "MANUEL", "MAURO", "MAURICIO",
	"NICOLAS", "ANTONIO", "RAFAEL", "ALBERTO", "ALVARO", "ADRIAN", "DIEGO", "RAUL"};
	
	public final static String nombresM[]= {"MARIA", "CARMEN", "JOSEFA", "ISABEL", "LAURA", "PILAR", "RAQUEL", "SARA", "PAULA",
			"ELENA", "LUCIA", "MARTA", "ANA", "CRISTINA", "DOLORES", "ROSARIO", "JULIA"};
	
	
	
	/**
	 * 
	 */
	public Nombres() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String randomNameH() {
	    int pick = aleatorioI	(0, nombresH.length-1);
	    return nombresH[pick];
	}
	
	public String randomNameM() {
	    int pick = aleatorioI(0, nombresM.length-1);
	    return nombresM[pick];
	}
	
	public String randomName() {
		int pick = aleatorioI(0 , nombresH.length + nombresM.length - 2);
		if(pick >= nombresH.length) {
			pick -= nombresH.length - 1;
			return nombresM[pick];
		}else
			return nombresH[pick];
	}
	public int aleatorioI(int min, int max) {
		int num = (int)(Math.random()*(max-min+1)+min);		
		return num;
	}
	public double aleatorioD(double min,double max) {
		double num = (Math.random()*(max-min+1)+min);		
		return num;
	}
}
