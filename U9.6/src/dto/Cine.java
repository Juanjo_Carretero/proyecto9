package dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;


public class Cine {
	private final static int FILAS[]= {1,2,3,4,5,6,7,8};
	private final static char COLUMNAS[] = {'A','B','C','D','E','F','G','H','I'};
	
	private Pelicula pelicula;
	private double precio;
	private HashMap<String, Boolean> asientos;
	
	
	
	/**
	 * 
	 */
	public Cine() {
		super();
		this.pelicula=null;
		this.precio=3.5;
		this.asientos= generaCine();
		// TODO Auto-generated constructor stub
	}



	/**
	 * @param pelicula
	 * @param precio
	 */
	public Cine(Pelicula pelicula, double precio) {
		super();
		this.pelicula = pelicula;
		if(precio>0)
			this.precio = precio;
		else
			this.precio = 3.5;
		this.asientos= generaCine();
	}



	private HashMap<String, Boolean> generaCine(){
		HashMap<String, Boolean> asientos = new HashMap<String, Boolean>();
		for(int i=0; i < FILAS.length; i++) {
			for(int j=0; j < COLUMNAS.length; j++) {
				asientos.put(String.valueOf(COLUMNAS[j])+String.valueOf(FILAS[i]), false);
			}
		}
		return asientos;
	}
	
	public void realizaSimulacion(ArrayList<Espectador> esp) {
		vaciaCine(this.asientos);
		ArrayList<Espectador> noEspacio=new ArrayList<Espectador>();
		ArrayList<Espectador> sinDineroEdad= new ArrayList<Espectador>();
		ArrayList<Espectador> entrado= new ArrayList<Espectador>();
		for(int i=0;i<esp.size();i++) {
			if(!cineLleno()) {
				if(cobraEntrada(esp.get(i))) {
					String asiento = asientoAleatorio();
					if(sientaEspectador(asiento)) {
						esp.get(i).setAsiento(asiento);
						entrado.add(esp.get(i));
					}
				}else sinDineroEdad.add(esp.get(i));
			}else noEspacio.add(esp.get(i));
		}
		mostrarResultados(sinDineroEdad,noEspacio,entrado,esp.size());
	}
	
	private void mostrarResultados(ArrayList<Espectador> sinDinero, ArrayList<Espectador> noEspacio,ArrayList<Espectador> entrado, int size) {
		System.out.println("De los "+size+" espectadores que querian ver la pelicula, "+(sinDinero.size()+noEspacio.size())
				+" no han podio entrar.\n\nPrecio pelicula: "+String.format("%.2f", precio)+"\nEdad minima: "+pelicula.getMinAge());
		System.out.println("\n\nESPECTADORES SIN DINERO SUFICIENTE O CON EDAD INSUFICIENTE ("+sinDinero.size()+") :\n"+sinDinero.toString());
		System.out.println("\n\nESPECTADORES QUE SE HAN QUEDADO SIN ESPACIO ("+noEspacio.size()+") :\n"+noEspacio.toString());
		System.out.println("\n\nESPECTADORES QUE HAN ENTRADO ("+(size-(sinDinero.size()+noEspacio.size()))+") : \n"+entrado.toString());

	}



	public void vaciaCine(HashMap<String, Boolean> asientos) {
		for(int i=0; i < FILAS.length; i++) {
			for(int j=0; j < COLUMNAS.length; j++) {
				asientos.replace(asiento(i, j), false);
			}
		}
	}
	
	
	
	public boolean cobraEntrada(Espectador e) {
		if(e.getDinero()> precio)
			return true;
		return false;
	}
	
	public boolean sientaEspectador(String asiento) {
		try {
			if(!asientos.get(asiento)) {
				asientos.replace(asiento, true);
				return true;
			}
		}catch(NullPointerException e) {
			
		}
		return false;
	}
	
	public String asiento(int fila, int columna) {
		return(String.valueOf(COLUMNAS[columna])+String.valueOf(FILAS[fila]));
	}
	
	public boolean cineLleno() {
		if(asientos.containsValue(false)) 
			return false;
		return true;
		
	}
	
	public String asientoAleatorio() {
		Nombres n=new Nombres();
		ArrayList<String> libres = obtenerAsientosLibre();
		if(libres.size()>0) {
			int aleatorio = n.aleatorioI(0, libres.size()-1);
			return libres.get(aleatorio);
		}else return null;
	}
	
	public ArrayList<String> obtenerAsientosLibre() {
		HashMap<String, Boolean> map= new HashMap<String, Boolean>();
		ArrayList<String> libres= new ArrayList<String>();
		for (int i=0; i < FILAS.length; i++) {
			for(int j=0;j < COLUMNAS.length;j++) {
				if(asientos.get(asiento(i,j)).equals(false))
					libres.add(asiento(i, j));
			}
		}
		return libres;
	}
	
	
}
