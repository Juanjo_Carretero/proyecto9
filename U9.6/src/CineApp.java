import java.util.ArrayList;

import dto.Cine;
import dto.Espectador;
import dto.Nombres;
import dto.Pelicula;

public class CineApp {
	private final static int ESPECTADORES=150;

	public static void main(String[] args) {
		Nombres n = new Nombres();
		ArrayList<Espectador> espectadores = generaEspectadores(ESPECTADORES);
		Pelicula p= new Pelicula("Los juegos el hambre", 140, 10, "John Philips");
		Cine c= new Cine(p,n.aleatorioD(1, 7));
		c.realizaSimulacion(espectadores);

	}
	
	
	
	public static ArrayList<Espectador> generaEspectadores(int num){
		Nombres n= new Nombres();
		ArrayList<Espectador> cola = new ArrayList<Espectador>();
		for(int i=0; i <num;i++ ) {
			cola.add(new Espectador(n.randomName(), n.aleatorioI(7,30), n.aleatorioD(0, 10)));
		}
		return cola;
	}

}
