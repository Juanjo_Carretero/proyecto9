package dto;
import java.lang.Math;

public class Raices {
	
	private double a;
	private double b;
	private double c;
	
	/**
	 * Constructor default
	 */
	public Raices() {
		super();
		this.a=1;
		this.b=2;
		this.c=3;
	}
	/**
	 * @param a: primer factor
	 * @param b: segundo factor
	 * @param c: tercer factor
	 */
	public Raices(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}
	/**
	 * @return the a
	 */
	public double getA() {
		return a;
	}
	/**
	 * @param a the a to set
	 */
	public void setA(double a) {
		this.a = a;
	}
	/**
	 * @return the b
	 */
	public double getB() {
		return b;
	}
	/**
	 * @param b the b to set
	 */
	public void setB(double b) {
		this.b = b;
	}
	/**
	 * @return the c
	 */
	public double getC() {
		return c;
	}
	/**
	 * @param c the c to set
	 */
	public void setC(double c) {
		this.c = c;
	}
	
	/**
	 * Devuelve el discriminante (b^2 - 4ac)
	 * @return el discriminante
	 */
	public double getDiscriminante() {
		return(Math.pow(this.b, 2)-(4*this.a*this.c));
	}
	
	/**
	 * Dice si la ecuacion tiene 2 soluciones
	 * @return cierto si tiene 2 o falso si no
	 */
	public boolean tieneRaices() {
		if(getDiscriminante()>0)
			return true;
		return false;
	}
	
	/**
	 * Sabiendo que la ecuacion tiene dos raices,
	 * las calcula y las muestra por pantalla.
	 */
	public void obtenerRaices() {
		double discriminante = getDiscriminante();
		double r1 = (-this.b+Math.sqrt(discriminante))/(2*a);
		double r2 = (-this.b-Math.sqrt(discriminante))/(2*a);
		System.out.println(r1+", "+r2+"\n");
	}
	
	/**
	 * Dice si la ecuacion tiene una y solo una raiz
	 * @return cierto si tiene1 o falso si no
	 */
	public boolean tieneRaiz() {
		if(getDiscriminante()==0)
			return true;
		return false;
	}
	
	/**
	 * Sabiendo que la ecuacion tiene una sola raiz,
	 * la calcula y la muestra por pantalla.
	 */
	public void obtenerRaiz() {
		double r1 = (-this.b)/(2*a);
		System.out.println(r1+"\n");
	}
	
	/**
	 * Dada una ecuacion de segundo grado a calcula.
	 */
	public void calcular() {
		if(tieneRaices())
			obtenerRaices();
		else {
			if(tieneRaiz())
				obtenerRaiz();
			else
				System.out.println("La ecuacion "+this.a+"x^2 + "+this.b+" x + "+this.c+" = 0  no tiene raices.\n");
		}
				
	}
	
	
	
	
	
	

}
