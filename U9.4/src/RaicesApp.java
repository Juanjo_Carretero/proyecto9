import dto.Raices;

public class RaicesApp {

	public static void main(String[] args) {
		Raices r1= new Raices(1, 5, 1); //2 RAICES
		Raices r2= new Raices(1,4,4); //1 SOLA RAIZ
		Raices r3= new Raices(4, 3, 2); //SIN RAICES
		
		r1.calcular();
		r2.calcular();
		r3.calcular();

	}

}