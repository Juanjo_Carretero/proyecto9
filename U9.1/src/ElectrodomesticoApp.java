import java.util.ArrayList;

import dto.Electrodomestico;
import dto.Lavadora;
import dto.Television;

public class ElectrodomesticoApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Electrodomestico> array=new ArrayList<Electrodomestico>();
		
		array.add(new Electrodomestico(10,"blanco",'C',20)); //Precio final 120
		array.add(new Electrodomestico(10,"blanco",'A',20)); //Precio final 160
		array.add(new Electrodomestico(10,"blanco",'B',35)); //Precio final 140
		array.add(new Electrodomestico(10,"blanco",'B',100)); //Precio final 190
		array.add(new Electrodomestico(10,"blanco",'C',63.2)); //Precio final 150
		array.add(new Lavadora(10,"blanco",'C',63.2,8)); //Precio final 150
		array.add(new Lavadora(10,"blanco",'C',63.2,35)); //Precio final 200
		array.add(new Television(10,"blanco",'C',63.2,20,false)); //Precio final 150
		array.add(new Television(10,"blanco",'C',63.2,45,false)); //Precio final 153
		array.add(new Television(10,"blanco",'C',63.2,20,true)); //Precio final  200
	
		/*
		 * ELECTRODOMESTICOS: 1613
		 * LAVADORA: 350
		 * TELEVISION: 503
		 */
		
		double priceE=0;double priceL=0;double priceT=0;double price=0;
		for(int i=0;i<array.size();i++) {
			if(array.get(i) instanceof Lavadora) {
				price= array.get(i).precioFinal();
				priceL+=price;
				priceE+=price;
			}else { 
				if(array.get(i) instanceof Television) {
					price= array.get(i).precioFinal();
					priceT+=price;
					priceE+=price;
				}else
					priceE+= array.get(i).precioFinal();
			}	
		}
		
		System.out.println("Electrodomesticos: "+priceE+" �\n"
				+ "Lavadoras: "+priceL+" �\n"
				+ "Televisores: "+priceT+" �\n");
	}
	

}

