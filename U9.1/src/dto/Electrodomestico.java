package dto;

/**
 * 
 * @author Marc Infante
 *
 */
public class Electrodomestico {
	private final static double DEF_BASE=100;
	private final static char CONSUMO[]= {'A','B','C','D','E','F'};
	private final static double DEF_PESO=5;
	private final static String COLORES[]= {"blanco","negro","rojo","azul","gris"};
	
	protected double precio_base;
	protected String color;
	protected char consumo_energetico;
	protected double peso;
	/**
	 * Constructor por defecto
	 */
	public Electrodomestico() {
		super();
		this.precio_base=DEF_BASE;
		this.color=COLORES[0];
		this.consumo_energetico=CONSUMO[5];
		this.peso=DEF_PESO;
	}
	/**
	 * @param precio_base: precio del producto
	 * @param peso: peso del producto
	 */
	public Electrodomestico(double precio_base, double peso) {
		super();
		this.precio_base = precio_base;
		this.color=COLORES[0];
		this.consumo_energetico=CONSUMO[5];
		this.peso = peso;
	}
	/**
	 * @param precio_base: precio del producto
	 * @param color: blanco, negro, rojo, azul o gris
	 * @param consumo_energetico: consumo del producto
	 * @param peso: peso del producto
	 */
	public Electrodomestico(double precio_base, String color, char consumo_energetico, double peso) {
		super();
		if(precio_base>0)
			this.precio_base = precio_base;
		else 
			this.precio_base = 0 ;
		if(comprobarColor(color))
			this.color = color.toLowerCase();
		else
			this.color = COLORES[0];
		if(comprobarConsumoEnergetico(consumo_energetico)!=-1)
			this.consumo_energetico = toMayus(consumo_energetico);
		else
			this.consumo_energetico = CONSUMO[5];
		if(peso>0)
			this.peso = peso;
		else
			this.peso = 0;
	}

	
	
	
	/**
	 * @return the precio_base
	 */
	public double getPrecio_base() {
		return precio_base;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @return the consumo_energetico
	 */
	public char getConsumo_energetico() {
		return consumo_energetico;
	}
	/**
	 * @return the peso
	 */
	public double getPeso() {
		return peso;
	}
	
	
	/**
	 *Dado un string que contiene un color mira si 
	 *esta en la lista de colores disponibles
	 * @param color: color dado por el usuario
	 * @return cierto si el color esta o falso si no
	 */
	private boolean comprobarColor(String color) {
		// TODO Auto-generated method stub
		for(int i=0; i< COLORES.length; i++) {
			if(COLORES[i].equals(color.toLowerCase()))
				return true;
		}
			
		return false;
	}
	

	/**
	 *Dado un char que contiene el consumo energetico
	 *comprueba que sea una letra correcta
	 * @param letra: char con el consumo
	 * (letra de la A a la F)
	 * @return -1 en caso de error y sino el indice de la lista
	 * del consumo
	 */
	private int comprobarConsumoEnergetico(char letra) {
		
		for(int i=0; i< CONSUMO.length; i++) {
			if(String.valueOf(CONSUMO[i]).equals(String.valueOf(letra).toUpperCase()))
				return i;
		}
		return -1;
	}
	

	
	
	/**
	 * Dado un char lo pasa a mayuscula y lo devuelve
	 * @param letra: letra a mayuscula
	 * @return char con la letra mayuscula
	 */
	private char toMayus(char letra) {
		String aux = String.valueOf(letra).toUpperCase();
		return(aux.charAt(0));
	}
	
	public double precioFinal() {
		int indice = comprobarConsumoEnergetico(this.consumo_energetico);
		double precioFinal= this.precio_base;
		switch (indice) {
		case 0:
			precioFinal+=100;
			break;
		case 1:
			precioFinal+=80;
			break;
		case 2:
			precioFinal+=60;
			break;
		case 3:
			precioFinal+=50;
			break;
		case 4:
			precioFinal+=30;
			break;
		case 5:
			precioFinal+=10;
			break;
		}
		
		if(this.peso>= 0 &&  peso < 20)
			precioFinal+=10;
		else{
			if(this.peso>= 20 &&  peso < 50)
				precioFinal+=50;
			else {
				if(this.peso>= 50 &&  peso < 80)
					precioFinal+=80;
				else {
					if(this.peso>= 80)
						precioFinal+=100;
				}
			}
		}
		return precioFinal;
	
	}
	
	
	
	
	
	
	
	
}
