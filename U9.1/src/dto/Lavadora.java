package dto;

public class Lavadora extends Electrodomestico {
	private final static int CARGA_DEF=5;
	
	private int carga;

	/**
	 * 
	 */
	public Lavadora() {
		super();
		this.carga=CARGA_DEF;
	}

	/**
	 * @param precio_base: precio base del producto
	 * @param peso: peso del produto
	 */
	public Lavadora(double precio_base, double peso) {
		super(precio_base, peso);
		this.carga=CARGA_DEF;
	}

	/**
	 * @param precio_base: precio del producto
	 * @param color: blanco, negro, rojo, azul o gris
	 * @param consumo_energetico: consumo del producto
	 * @param peso: peso del producto
	 * @param carga: carga que soporta la lavadora
	 */
	public Lavadora(double precio_base, String color, char consumo_energetico, double peso,int carga) {
		super(precio_base, color, consumo_energetico, peso);
		if(carga>0)
			this.carga=carga;
		else
			this.carga=CARGA_DEF;
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the carga
	 */
	public int getCarga() {
		return carga;
	}
	
	@Override
	public double precioFinal() {
		double precioFinal = super.precioFinal();
		if(this.carga>30)
			precioFinal+=50;
		return precioFinal;
	}
	

}
