package dto;

public class Television extends Electrodomestico {
		private final static int RESOLUTION_DEF=20;
		
		private int resolucion;
		private boolean sintonizadorTDT;
		/**
		 * 
		 */
		public Television() {
			super();
			this.resolucion=RESOLUTION_DEF;
			this.sintonizadorTDT=false;
		}
		/**
		 * @param precio_base: precio del producto
		 * @param peso: peso del producto
		 */
		public Television(double precio_base, double peso) {
			super(precio_base, peso);
			this.resolucion=RESOLUTION_DEF;
			this.sintonizadorTDT=false;
		}
		/**
		 * @param precio_base: precio del producto
		 * @param color: blanco, negro, rojo, azul o gris
		 * @param consumo_energetico: consumo del producto
		 * @param peso: peso del producto
		 * @param resolucion: pulgadas del televisor
		 * @param sintonizador: si tiene sintonizador cierto
		 * y falso en el caso contrario
		 */
		public Television(double precio_base, String color, char consumo_energetico, double peso, int resolucion,boolean sintonizador) {
			super(precio_base, color, consumo_energetico, peso);
			if(resolucion>0) 
				this.resolucion=resolucion;
			this.sintonizadorTDT=sintonizador;
			// TODO Auto-generated constructor stub
		}
		/**
		 * @return the resolucion
		 */
		public int getResolucion() {
			return resolucion;
		}
		/**
		 * @return the sintonizadorTDT
		 */
		public boolean isSintonizadorTDT() {
			return sintonizadorTDT;
		}
		
		@Override
		public double precioFinal() {
			double precioFinal = super.precioFinal();
			if(this.resolucion>40)
				precioFinal+=(0.3*this.precio_base);
			if(isSintonizadorTDT())
				precioFinal+=50;
			return precioFinal;
		}
		
		
}
