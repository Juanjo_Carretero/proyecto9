import java.util.ArrayList;

import Model.Aula;

public class SimulationApp {
	
	private final static int NUM_SIMULACIONES=30;
	private final static int TAMA�O_AULA=60;

	public static void main(String[] args) {
		//GENERAMOS NUM_SIMULACIONES AULAS
		ArrayList<Aula> colegio = new ArrayList<Aula>();
		for(int i=0;i<NUM_SIMULACIONES;i++) {
			if(i<NUM_SIMULACIONES/3)
				colegio.add(new Aula(TAMA�O_AULA, "matematicas"));
			if(i>=NUM_SIMULACIONES/3 && i<(NUM_SIMULACIONES/3)*2)
				colegio.add(new Aula(TAMA�O_AULA, "fisica"));
			if(i>=(NUM_SIMULACIONES/3)*2)
				colegio.add(new Aula(TAMA�O_AULA, "filosofia"));
		}
		
		//REALIZAMOS LA PRUEBA CON CADA UNA DE ELLAS
		for(int i=0; i < NUM_SIMULACIONES;i++) {
			System.out.println(colegio.get(i).toString());
			colegio.get(i).realizaSimulacion();
		}
	}

}