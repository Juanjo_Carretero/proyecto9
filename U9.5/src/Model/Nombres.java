package Model;

public class Nombres {

	public final static char HOMBRE='H';
	public final static char MUJER='M';
	
	public final static String nombresH[]= {"MARC", "DANIEL", "JESUS", "JOSE", "JUAN", "PEDRO", "MANUEL", "MAURO", "MAURICIO",
	"NICOLAS", "ANTONIO", "RAFAEL", "ALBERTO", "ALVARO", "ADRIAN", "DIEGO", "RAUL"};
	
	public final static String nombresM[]= {"MARIA", "CARMEN", "JOSEFA", "ISABEL", "LAURA", "PILAR", "RAQUEL", "SARA", "PAULA",
			"ELENA", "LUCIA", "MARTA", "ANA", "CRISTINA", "DOLORES", "ROSARIO", "JULIA"};
	
	
	
	/**
	 * 
	 */
	public Nombres() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String randomNameH() {
	    int pick = aleatorio(0, nombresH.length-1);
	    return nombresH[pick];
	}
	
	public String randomNameM() {
	    int pick = aleatorio(0, nombresM.length-1);
	    return nombresM[pick];
	}
	
	public int aleatorio(int min, int max) {
		int num = (int)(Math.random()*(max-min+1)+min);		
		return num;
	}
}
