package Model;

public class Alumno extends Persona {

	private double calificacion;
	
	public Alumno() {
		super("NAME",15,'H');
		this.calificacion=aleatorioD(0, 10);
	}
	
	/**
	 * 
	 */
	public Alumno(String name, int age, char sexo,double calificacion) {
		super(name,age,sexo);
		if(age < 12 || age > 18)
			this.setAge(aleatorio(6, 24));
		if(calificacion>=0 && calificacion<=10)
			this.calificacion=calificacion;
		else
			this.calificacion=aleatorio(0, 10);
	}

	

	/**
	 * @return the calificacion
	 */
	public double getCalificacion() {
		return calificacion;
	}

	/**
	 * @param calificacion the calificacion to set
	 */
	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	@Override
	public boolean probFalta() {
		if(aleatorio(1, 100)>50)
			return true;
		return false;
	}



	@Override
	public String toString() {
		return "["+super.toString()+" calificacion=" + String.format("%.2f",calificacion) + "]";
	}
	
	
	/**
	 * Genera un numero aleatorio decimal entre min y 
	 * max ambos incluidos 
	 * @param min: rango minimo
	 * @param max: rango maximo
	 * @return numero decimal aleatorio en el rango
	 */
	public double aleatorioD(double min,double max) {
		double num = (Math.random()*(max-min+1)+min);		
		return num;
	}
	

}
