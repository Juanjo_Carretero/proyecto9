package Model;
import java.lang.Math;

public abstract class Persona {
		protected final static char HOMBRE='H';
		protected final static char MUJER='M';
		
		protected String name;
		protected int age;
		protected char sexo;






		/**
		 * @param name: nombre de la persona
		 * @param age: edad de la persona
		 * @param sexo: sexo de la persona
		 */
		public Persona(String name, int age, char sexo) {
			super();
			this.name = name;
			if(age>0)
				this.age = age;
			else
				this.age = 20;
			if(String.valueOf(sexo).toUpperCase().equals(String.valueOf(HOMBRE))
					|| String.valueOf(sexo).toUpperCase().equals(String.valueOf(MUJER)))
				this.sexo = String.valueOf(sexo).toUpperCase().charAt(0);
			else
				this.sexo = HOMBRE;
		
		}
		
		



		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}





		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}





		/**
		 * @return the age
		 */
		public int getAge() {
			return age;
		}





		/**
		 * @param age the age to set
		 */
		public void setAge(int age) {
			if(age>0)
				this.age = age;
			else
				this.age = 20;
		}





		/**
		 * @return the sexo
		 */
		public char getSexo() {
			return sexo;
		}





		/**
		 * @param sexo the sexo to set
		 */
		public void setSexo(char sexo) {
			if(String.valueOf(sexo).toUpperCase().equals(String.valueOf(HOMBRE))
					|| String.valueOf(sexo).toUpperCase().equals(String.valueOf(MUJER)))
				this.sexo = sexo;
			else
				this.sexo = HOMBRE;
		}





		public abstract boolean probFalta();
		
		
		/**
		 * Genera un numero aleatorio entre min y max ambos incluidos
		 * @param min: rango minimo
		 * @param max: rango maximo
		 * @return un numero aleatorio dentro del rango
		 */
		public int aleatorio(int min, int max) {
			int num = (int)(Math.random()*(max-min+1)+min);		
			return num;
		}





		@Override
		public String toString() {
			return "name=" + name + ", age=" + age + ", sexo=" + sexo ;
		}
		
		
}
