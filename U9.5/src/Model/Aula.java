package Model;

import java.util.ArrayList;


public class Aula {
	private final static String MATERIAS[]= {"matematicas","fisica","filosofia"};
	private static int numAula;
	private int maxEstudiantes;
	private int id;
	private String materia;
	private ArrayList<Alumno> listaAula;
	private Profesor profe;
	
	
	
	
	/**
	 * 
	 */
	public Aula() {
		super();
		this.maxEstudiantes=aleatorioI(20,50);;
		this.id=this.numAula;
		this.numAula++;
		this.materia=MATERIAS[0];
		this.profe = profealeatorio();
		this.listaAula = generaAlumnos(this.maxEstudiantes);
	}
	
	/**
	 * Se genera un profesor con atributos aleatorios
	 * y se generan maxEstudiantes con atributos aleatorios
	 * @param maxEstudiantes: numero de estudiantes del aula
	 * @param materia: materia que se impartira el aula
	 */
	public Aula(int maxEstudiantes, String materia) {
		super();
		if(maxEstudiantes>0) 
			this.maxEstudiantes = maxEstudiantes;
		else
			this.maxEstudiantes = aleatorioI(20,50);
		this.id=this.getNumAula();
		this.setNumAula(this.id+1);
		if(existMateria(materia))
			this.materia = materia;
		else
			this.materia = MATERIAS[0];
		this.listaAula = generaAlumnos(maxEstudiantes);
		this.profe = profealeatorio();
	}

	
	
	
	/**
	 * @return the numAula
	 */
	public static int getNumAula() {
		return numAula;
	}

	/**
	 * @param numAula the numAula to set
	 */
	public static void setNumAula(int numAula) {
		Aula.numAula = numAula;
	}

	/**
	 * @param maxEstudiantes: numero de estudiantes del aula
	 * @param materia: materia que se impartira el aula
	 * @param listaAula: ArrayList de alumnos del aula
	 * @param profe: Profesor del aula
	 */
	public Aula(int maxEstudiantes, String materia, ArrayList<Alumno> listaAula, Profesor profe) {
		super();
		this.maxEstudiantes = maxEstudiantes;
		this.materia = materia;
		this.listaAula = listaAula;
		this.profe = profe;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Dado un numero de alumnos que se quieren
	 * devuleve un arraylist inicializado con num
	 * alumnos aleatorios
	 * @param num: numero de alumnos en el arraylist
	 * @return arraylist inicializado con num alumnos
	 */
	private ArrayList<Alumno> generaAlumnos(int num) {
		ArrayList<Alumno> aux= new ArrayList<Alumno>();
		int edad= aleatorioI(6, 24);
		Nombres n=new Nombres();
		for(int i=0; i< num;i++) {
			if(aleatorioI(1,2)==1) aux.add(new Alumno(n.randomNameH(), edad, 'H', aleatorioD(0, 10)));
			else aux.add(new Alumno(n.randomNameM(), edad, 'M', aleatorioD(0, 10)));
		}
		return aux;
	}

	
	/**
	 * Crea un Profesor aleatorio y lo devuelve por
	 * parametro.
	 * @return un Profesor con atributos aleatorios
	 */
	private Profesor profealeatorio() {
		// TODO Auto-generated method stub
		int materia= aleatorioI(0, 2);
		int edad= aleatorioI(25, 65);
		Nombres n = new Nombres();
		if (aleatorioI(1,2) == 2) return(new Profesor(n.randomNameH(),edad,'H',MATERIAS[materia]));
		else return(new Profesor(n.randomNameM(),edad,'M',MATERIAS[materia]));
	}


	/**
	 * Genera un numero aleatorio entero entre min y 
	 * max ambos incluidos
	 * @param min: rango minimo
	 * @param max: rango maximo
	 * @return un numero aleatorio dentro del rango
	 */
	public int aleatorioI(int min, int max) {
		int num = (int)(Math.random()*(max-min+1)+min);		
		return num;
	}
	
	/**
	 * Genera un numero aleatorio decimal entre min y 
	 * max ambos incluidos 
	 * @param min: rango minimo
	 * @param max: rango maximo
	 * @return numero decimal aleatorio en el rango
	 */
	public double aleatorioD(double min,double max) {
		double num = (Math.random()*(max-min+1)+min);		
		return num;
	}

	/**
	 * Comprueba si el string esta en la lista
	 * de materias.
	 * @param materia: string a comprobar
	 * @return cierto si esta o falso si no.
	 */
	public boolean existMateria(String materia) {
		for(String mat : MATERIAS) {
			if(mat.equals(materia.toLowerCase()))
				return true;
		}
		return false;
	}
	
	/**
	 * Simula si se puede realizar una clase
	 * suponiendo que el profesor faltara el 30%
	 * de las veces, los alumnos un 50% y ademas
	 * el profesor tiene que ser de la misma 
	 * materia que el aula.
	 */
	public void realizaSimulacion() {
		//Controlar si se puede
		if(controlAsistencia()) {
			System.out.println("Han asistido los alumnos necesarios.");
			if(controlProfesor()) {
				System.out.println("El profesor puede dar clase y es de la materia.\n");
				showAprobXSexo();
			}else
				System.out.println("El profesor NO daba "+this.materia+" o NO ha podido venir.");
			
		}else
			System.out.println("NO han asistido suficientes alumnos.");
		System.out.println("\n\n");
	}

	private void showAprobXSexo() {
		String  aprobM="";String aprobH="";
		for(int i = 0 ; i < listaAula.size() ; i++ ) {
			if(listaAula.get(i).getCalificacion() > 5) {
				if(listaAula.get(i).getSexo() =='H')
					aprobH+=listaAula.get(i).getName()+" ha sacado : "+String.format("%.2f",listaAula.get(i).getCalificacion())+"\n";
				else
					aprobM+=listaAula.get(i).getName()+" ha sacado : "+String.format("%.2f",listaAula.get(i).getCalificacion())+"\n";
			}
		}
		System.out.println("MUJERES APROBADAS:\n"+aprobM+"\nHOMBRES APROBADOS:\n"+aprobH);
		
		
	}

	/**
	 * Controla que el profesor asista a clase y ademas
	 * si imparte la materia del aula
	 * @return cierto si las dos condiciones son ciertas o 
	 * falso si no
	 */
	private boolean controlProfesor() {
		if(!profe.probFalta() && profe.getMateria().equals(materia)) 
				return true;
		return false;
	}

	/**
	 * Controla que almenos la mitad de la clase vaya
	 * @return cierto si va mas de la mitad de las personas
	 * y falso si no
	 */
	private boolean controlAsistencia() {
		int numAsistentes=0;
		for(int i=0; i<listaAula.size();i++) {
			if(!listaAula.get(i).probFalta())
				numAsistentes++;
		}
		if(numAsistentes> (listaAula.size()/2))
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "Aula "+id+" :\n  Estudiantes:" + maxEstudiantes + "\n  Materia:" + materia +"\n  Profesor:" + profe.toString() + "\n  Alumnos: " + listaAula+"\n" ;
	}
	
	
}
