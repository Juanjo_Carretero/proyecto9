package Model;


public class Profesor extends Persona {
	private final static String MATERIAS[]= {"matematicas","filosofia","fisica"};
	
	private String materia;
	
	
	
	public Profesor() {
		super("NAME", 50, 'M');
		materia=MATERIAS[0];
	}
	
	public Profesor(String name, int age, char sexo,String materia) {
		super(name, age, sexo);
		if(age<25 || age > 65)
			this.setAge(aleatorio(25, 65));
		if(existMateria(materia))
			this.materia=materia;
		else
			materia=MATERIAS[0];
	}
	
	


	/**
	 * @return the materia
	 */
	public String getMateria() {
		return materia;
	}

	/**
	 * @param materia the materia to set
	 */
	public void setMateria(String materia) {
		this.materia = materia;
	}

	@Override
	public boolean probFalta() {
		if(aleatorio(1, 100)>70)
			return true;
		return false;
	}


	public boolean existMateria(String materia) {
		for(String mat : MATERIAS) {
			if(mat.equals(materia.toLowerCase()))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "["+super.toString()+" materia=" + materia + "]";
	}
	
	

}
